<?php
    
    // Create connection
    $conn = mysqli_connect("localhost", "root", "", "bank management system");

    // Fetch Data
    $sql = mysqli_query($conn, "Select * From customer");

    // Store data in a result variable
    $result = mysqli_fetch_all($sql, MYSQLI_ASSOC);
    
    // Gives the result to the fetch API
    exit(json_encode($result));

?>
